const express = require('express');
const Discord = require('discord.js');
const fs      = require('fs');
const request = require('request');
const cheerio = require('cheerio');
const app     = express();

exports.run = m => {

const prefix = config.prefix.split("").map(c => `\\${c}`).join("");
let args = m.content.match(new RegExp(`^(?:<@!?${bot.user.id}> |${config.prefix})((?:.|[^.])+)`, 'i'));
if (!args) return;

args = args[1].split(/ +/);
let command = args.shift().toLowerCase();

const commands = fs.readdirSync('./commands/').filter(c => c.endsWith(".js")).map(c => c.slice(0, -3));
const lcCommands = commands.map(c => c.toLowerCase());
const index = lcCommands.indexOf(command);

if (index === -1) return;
const cmdModule = rq(`./commands/${commands[index]}.js`);
const options = typeof cmdModule.options === "object" && String(cmdModule.options) === "[object Object]" ? cmdModule.options : {};

if (options.disabled === true) return;
if (options.ownerOnly === true && config.owners.indexOf(m.author.id) === -1) return m.channel.send("This command is for bot owners only");

const commandData = {
    args: args,
    prefix: prefix
};

try {
    cmdModule.run(m, commandData);
}
catch (e) {
    console.error(e);
    m.channel.send("An unexpected error has occured while running the command. Sent to Developers.");
}
};