const express = require('express');
const Discord = require('discord.js');
const fs      = require('fs');
const request = require('request');
const cheerio = require('cheerio');
const app     = express();

exports.run = (m, data) => {
    let commands = fs.readdirSync('./commands').filter(c => c.endsWith('.js')).map(c => c.slice(0, -3));
    let lcCommands = commands.map(c => c.toLowerCase());

    if(data.args.length === 0) {
        let help = {};
        commands.forEach(c => {
            let command = rq(`./commands/${c}.js`);
            if (typeof command.help !== "object" && String(command.help) !== '[object Object]') command.help = {};
            if (typeof command.help.category !== "string") command.help.category = "No Category";
            if (typeof command.help.usage !== "string") command.help.usage = "";
            if (typeof command.help.description !== "string") command.help.description = "No Description";
            
            if(!help.hasOwnProperty(command.help.category)) help[command.help.category] = [];
            help[command.help.category].push({ name: `${config.prefix}${c} ${command.help.usage}`, value: `${command.help.description}`, inline: false});
        });
        
        let generateEmbed = (category, continued) => ({ title: `Help - ${category}${continued ? " (Continued)" : ""}`, color: 3447003, fields: [] });
        let messages = [];

        for (let i in help) {
            if (help.hasOwnProperty(i)) {
                const helpData = help[i];
                messages.push(generateEmbed(i, false));

                helpData.forEach(h => {
                    if (messages[messages.length - 1].fields.length === 25) messages.push(generateEmbed(i, true));

                    messages[messages.length - 1].fields.push(h);
                });
            } 
        }

        const sendMessage = i => {
            m.author.send({ embed: messages[i] }).then(() => {
                i++;
                if(i === messages.length) {
                    if(m.channel.type === 'text') m.channel.send('Help is on its way! Check your DMs!');
                }
                else sendMessage(i);
            }).catch(() => {
                m.channel.send('Error sending help! Make sure I am not blocked and DMs are enabled in the server.');
            });
        };

        sendMessage(0);
    }
    else {
        let index = lcCommands.indexOf(data.args[0].toLowerCase());
        if (index === -1) return m.channel.send('That command does not exist!');
        let command = rq(`./commands/${commands[index]}.js`);
        if (typeof command.help !== "object" && String(command.help) !== '[object Object]') command.help = {};
        if (typeof command.help.category !== "string") command.help.category = "No Category";
        if (typeof command.help.usage !== "string") command.help.usage = "";
        if (typeof command.help.description !== "string") command.help.description = "No Description";

       // m.channel.send(`Help for the ${commands[index]} command:\n\n**Category**: ${command.help.category}\n**Usage**: ${data.prefix}${commands[index]} ${command.help.usage}\n**Description**: ${command.help.description}`);


        m.channel.send({embed: {
            color: 3447003,
            title: `Help Command for ${commands[index]} command`,
            author: {
                name: bot.user.username,
                icon_url: bot.user.displayAvatarURL
            },
            fields: [{
                name: `Command Category:`,
                value: `${command.help.category}`,
                inline: true
            },
            {
                name: `Command Usage:`,
                value: `fn/${commands[index]} ${command.help.usage}`.replace("\\", "").replace("\n", "n"),
            },
            {
                name: `Command Description:`,
                value: `${command.help.description}`,
            }
          ],
          timestamp: new Date()
         }
        });

    }
};

exports.help = {
    category: "General",
    usage: "[command]",
    description: "Gets help for all commands or a specified command."
};