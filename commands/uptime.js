const express = require('express');
const Discord = require('discord.js');
const fs      = require('fs');
const request = require('request');
const cheerio = require('cheerio');
const app     = express();

var hrString  = "";
var dyString  = "";
var mtString  = "";
var sdString  = "";

exports.run = (m, data) => {

    m.channel.send(`Calculating Uptime...`).then(msg => {

        if(utDay === 1) dyString = " day, "; else dyString = " days, ";
        if(utHour === 1) hrString = " hour, "; else hrString = " hours, ";
        if(utMin === 1) mtString = " minute, "; else mtString = " minutes, ";
        if(utSec === 1) sdString = " second"; else sdString = " seconds";

        msg.edit("```Bot Uptime: " + utDay + dyString + utHour + hrString + utMin + mtString + utSec + sdString + "```");
    });


}

exports.help = {
    category: "General",
    description: "Displays uptime of the bot for current session."
}