const express = require('express');
const Discord = require('discord.js');
const fs      = require('fs');
const request = require('request');
const cheerio = require('cheerio');
const app     = express();

var url = 'https://www.epicgames.com/fortnite/en-US/news'; 
var version, date;
var dataFile = "./output.json";  

exports.run = (m, data) => {
    var jsonFile = fs.readFileSync(dataFile);
    var parsedFile = JSON.parse(jsonFile);

    m.channel.send(`Current Fortnite Version: v${parsedFile.version}\nReleased On: ${parsedFile.date}`);
}

exports.help = {
    category: "Fortnite",
    description: "Gets current Fortnite version and release date."
}