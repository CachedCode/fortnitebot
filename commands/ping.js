const express = require('express');
const Discord = require('discord.js');
const fs      = require('fs');
const request = require('request');
const cheerio = require('cheerio');
const app     = express();

exports.run = (m, data) => {
    m.channel.send("Pong!").then(msg => {
        msg.edit(`Pong!\n \`\`\`API Ping: ${msg.createdTimestamp - m.createdTimestamp}ms\nAPI Heartbeat: ${Math.round(bot.ping)}ms \`\`\``);
    });

};

exports.help = {
    category: "General",
    description: "Pings the bot.",
};