const express = require('express');
const Discord = require('discord.js');
const fs      = require('fs');
const request = require('request');
const cheerio = require('cheerio');
const app     = express();

var currentGuildUsers, currentGuildChannels, currentGuildId, currentGuildName, currentGuildAvatar;

exports.run = (m, data) => {
    currentGuildId = m.guild.id;
    currentGuildName = m.guild.name;
    currentGuildUsers = m.guild.memberCount;
    currentGuildChannels = m.guild.channels.size;
    currentGuildAvatar = m.guild.iconURL;

    m.channel.send(`\`\`\`Calculating Information...\`\`\``).then(msg => {

        const embed = new Discord.RichEmbed()
        .setAuthor(bot.user.username, bot.user.displayAvatarURL)
        .setColor(0x5697ff)
        .setThumbnail(currentGuildAvatar)
        .setTimestamp()
        .addField(`Server Name:`, `${currentGuildName}`, true)
        .addField(`Server ID:`, `${currentGuildId}`, false)
        .addBlankField(true)
        .addField(`Server Channels:`, `${currentGuildChannels}`)
        .addField(`Server Users:`, `${currentGuildUsers}`);

        msg.edit({embed});

        }); // END OF MESSAGE EDIT
    }; // END OF MESSAGE

exports.help = {
    category: "General",
    description: "Gets User count of current guild."
};