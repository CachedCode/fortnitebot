const express = require('express');
const Discord = require('discord.js');
const fs      = require('fs');
const request = require('request');
const cheerio = require('cheerio');
const app     = express();
const asyncio = require('asyncio');

global.stHour = 0;
global.stMin  = 0;
global.stSec  = 0;

global.utDay  = 0;
global.utHour = 0;
global.utMin  = 0;
global.utSec  = 0;

global.config = require('./config.json')
global.bot    = new Discord.Client();

var url = 'https://www.epicgames.com/fortnite/en-US/news'; 

global.rq = f => {
    delete require.cache[require.resolve(f)];
    return require(f);
}

bot.login('NDI5Mjc0OTEzMzg4MzYzNzc2.DZ_UzQ.X5i_a-7r5sxyh4sVc2TxqGU8bv8');

bot.on('ready', function(e) {
    stHour = 0;
    stMin = 0;
    stSec = 0;
    displayStartTime();
    console.log(`Bot Connected! ${bot.users.size} users, in ${bot.channels.size} channels of ${bot.guilds.size} servers.`);
    console.log(`Bot started at time: ${stHour}:${stMin}:${stSec}`);
    if(bot.guilds.size === 1) {
        bot.user.setActivity(`fn/help on ${bot.guilds.size} server`);
    } else {
        bot.user.setActivity(`fn/help on ${bot.guilds.size} servers`);
    }
    grabVersion();
    calculateUptime();
});

bot.on("guildCreate", guild=> {
    console.log(`New guild added: ${guild.name} (id: ${guild.id}) This guild has ${guild.memberCount} members!`);

    bot.channels.get('429287920873242624').send({embed: {
        color: 0x5697ff,
        title: "Bot Added to Server!",
        author: {
            name: bot.user.username,
            icon_url: bot.user.displayAvatarURL
        },
        fields: [{
            name: `Server Name:`,
            value: `${guild.name}`,
            inline: true
        },
        {
            name: `Server ID:`,
            value: `${guild.id}`,
            inline: true
        },
        {
            name: `Server Member Count:`,
            value: `${guild.memberCount}`,
        },
        {
            name: `Server Owner:`,
            value: `${guild.owner.user.username} #${guild.owner.user.discriminator}`,
        }
      ],
      timestamp: new Date()
     }
    });

    bot.user.setActivity(`fn/help on ${bot.guilds.size} servers`);
});

bot.on("guildDelete", guild => {
    console.log(`Removed from guild: ${guild.name} (id: ${guild.id})`);

    bot.guilds.get('416572630418849792').channels.get('429287920873242624').send({embed: {
        color: 0xFF2D38,
        title: "Bot Removed From Server!",
        author: {
            name: bot.user.username,
            icon_url: bot.user.displayAvatarURL
        },
        fields: [{
            name: `Server Name:`,
            value: `${guild.name}`,
            inline: true
        },
        {
            name: `Server ID:`,
            value: `${guild.id}`,
            inline: true
        },
        {
            name: `Server Member Count:`,
            value: `${guild.memberCount}`,
        },
        {
            name: `Server Owner:`,
            value: `${guild.owner.user.username} #${guild.owner.user.discriminator}`,
        }
      ],
      timestamp: new Date()
     }
    });

    bot.user.setActivity(`fn/help on ${bot.guilds.size} servers`);
});

const oldEmit = bot.emit.bind(bot);
bot.emit = (event, ...args) => {
	const events = fs.readdirSync('./events').filter(e => e.endsWith(".js")).map(e => e.slice(0, -3));
	const lcEvents = events.map(e => e.toLowerCase());
	let index = lcEvents.indexOf(event.toLowerCase());

	if (index !== -1) {
		try {
			const event = rq(`./events/${events[index]}.js`);

			if (event.disabled !== false) event.run(...args);
		}
		catch (e) {
			console.error(e);
		}
	}

	oldEmit(event, ...args);
};

const calculateUptime = function() {
    var calcUt = setInterval (function() {
        utSec++;
        if(utSec >= 60) {
            utSec = 0;
            utMin++;
        }
        if(utMin >= 60) {
            utMin = 0;
            utHour++;
            console.log(`Hours Passed:` + utHour);
        }
        if(utHour >= 24) {
            utHour = 0;
            utDay++;
            console.log(`Days Passed: ${utDay}`);
        }
    }, 1 * 1000);
}

const grabVersion = function() {
    var minute = 60 * 1000;
    var hour = 60 * minute;
    var i = 0;

    var checkVer = setInterval (function () {
        request(url, function(error, respone, html) {
            if(!error) {
                var $ = cheerio.load(html);
    
                var json = { version: "", date: ""};
                
                $('.top-featured-activity').each(function(i, elem) {
                    var data = $(this);
                    version = data.find('h2.__post-title__').text();
                    date = data.find('span.__post-date__').text();
    
                    if (version.toLowerCase().indexOf("v") === 0) {
                    
                        version = version.replace("v", "").split(" ");
    
                        json.version = version[0];
                        json.date = date;
                    
                        //save into a json config file and load every time bot updates itself
                        fs.writeFile('output.json', JSON.stringify(json, null, 4), function(err) {
                            console.log('File Successfully Written!');
                        });
                    } else {
                        console.log(`No New Update! File not being changed.`);
                    }
                })
                
            }
        });
      }, hour); 
}

function displayStartTime() {
    var str = "";

    var currentTime = new Date();
    stHour = currentTime.getHours();
    stMin = currentTime.getMinutes();
    stSec = currentTime.getSeconds();

    if (stMin < 10) {
        stMin = "0" + stMin;
    }
    if (stSec < 10) {
        stSec = "0" + stSec;
    }
    str += stHour + ":" + stMin + ":" + stSec + " ";
    return str;
}


